#pragma once

#include <QObject>
#include <QDebug>
#include <QString>
#include <QAbstractItemModel>

#include "../../Interfaces/Middleware/idataextention.h"
#include "../../Interfaces/Utility/i_user_task_data_ext.h"
#include "../../Interfaces/Utility/i_user_task_image_data_ext.h"
#include "../../Interfaces/Middleware/DataExtentionBase/DataExtentionBase.h"
#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

class DataExtention : public QObject, public IUserTaskImageDataExtention
{
	Q_OBJECT
	Q_INTERFACES(IUserTaskImageDataExtention IDataExtention)
	DATA_EXTENTION_BASE_DEFINITIONS(IUserTaskImageDataExtention, IUserTaskDataExtention, {"image"})

public:
	DataExtention(QObject* parent, ReferenceInstancePtr<IUserTaskDataExtention> extention) :
		QObject(parent),
		m_extention(extention)
	{
	}

	virtual ~DataExtention() = default;

signals:
	void modelChanged() override;

private:
	// IUserTaskDataExtention interface
	QByteArray image() override
	{
		return QByteArray();
	}

	QString name() override
	{
		return m_extention->name();
	}

	bool isDone() override
	{
		return m_extention->isDone();
	}

private:
	ReferenceInstancePtr<IUserTaskDataExtention> m_extention;
};
